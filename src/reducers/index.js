import {combineReducers} from 'redux';
import taskReducer from './task.reducers';

const rootReducer = combineReducers ({
    taskReducer
})

export default rootReducer;