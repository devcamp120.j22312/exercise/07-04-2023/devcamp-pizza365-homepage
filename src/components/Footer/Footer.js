import { Container, Grid } from "@mui/material";
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import InstagramIcon from '@mui/icons-material/Instagram';

const Footer = () => {
    return (
        <Container maxWidth style={{ padding: "0", marginTop:"30px" }}>
            <Grid item style={{ height: "135px", backgroundColor: "orange" }}>
                <p style={{fontWeight:"600", textAlign:"center", paddingTop:"15px", fontSize:"24px"}}>Footer</p>
                <Grid container style={{display:"flex", justifyContent:"center", marginTop:"-16px"}}>   
                    <FacebookOutlinedIcon style={{margin:"5px"}} />
                    <TwitterIcon style={{margin:"5px"}}/>
                    <YouTubeIcon style={{margin:"5px"}}/>
                    <InstagramIcon style={{margin:"5px"}}/>
                </Grid>
                <p style={{textAlign:"center", fontWeight:"500", fontSize:"20px", marginTop:"5px"}}>Power by Devcamp120</p>
            </Grid>
        </Container>
    )
}

export default Footer;