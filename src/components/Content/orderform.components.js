import { Container, TextField, Typography, Button } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SubmitFormHandler, FormHandler } from "../../actions/task.actions";
import AlertDialog from "../body/modal";

const OrderFormComponent = () => {
    const {status} = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    // Khai báo dispatch để lấy giá trị
    const dispatch = useDispatch();
    const [inputValue, setValue] = useState({
        fullName: "",
        email: "",
        phone: "",
        address: "",
        voucher: "",
        message: ""
    })

    const handleChange = (event) =>{
        const value = event.target.value;
        setValue({
            ...inputValue,
            [event.target.name]: value
        });
        dispatch(FormHandler({
            ...inputValue,
            [event.target.name]: value
        }))
    }
    const onBtnSendFormHandler = () =>{
        dispatch(SubmitFormHandler())
    }

    return (
        <Container style={{ marginTop: "30px" }}>
            <Typography style={{ fontWeight: "700", textAlign: "center", color: "orange", fontSize: "28px" }}>
                GỬI ĐƠN HÀNG
            </Typography>
            <hr style={{ width: "20%" }} />
            <p style={{ fontWeight: "600", textAlign: "center", color: "#FFA500", fontSize: "18px", marginTop: "5px" }}>Vui lòng điền thông tin bên dưới</p>
            <Typography>Họ tên Khách hàng</Typography>
            <TextField fullWidth placeholder="Nhập tên" variant="outlined" style={{marginTop:"10px"}} value={inputValue.fullName} onChange={handleChange}></TextField>
            <Typography style={{marginTop:"10px"}}>Email</Typography>
            <TextField fullWidth placeholder="Nhập email" variant="outlined" style={{marginTop:"10px"}} value={inputValue.email} onChange={handleChange}></TextField>
            <Typography style={{marginTop:"10px"}}>Số điện thoại</Typography>
            <TextField fullWidth placeholder="Nhập số điện thoại" variant="outlined" style={{marginTop:"10px"}} value={inputValue.phone} onChange={handleChange}></TextField>
            <Typography style={{marginTop:"10px"}}>Địa chỉ</Typography>
            <TextField fullWidth placeholder="Nhập địa chỉ" variant="outlined" style={{marginTop:"10px"}} value={inputValue.address} onChange={handleChange}></TextField>
            <Typography style={{marginTop:"10px"}}>Mã giảm giá</Typography>
            <TextField fullWidth placeholder="Nhập mã giảm giá" variant="outlined" style={{marginTop:"10px"}} value={inputValue.voucher} onChange={handleChange}></TextField>
            <Typography style={{marginTop:"10px"}}>Lời nhắn</Typography>
            <TextField fullWidth placeholder="Nhập ghi chú" variant="outlined" style={{marginTop:"10px"}} value={inputValue.message} onChange={handleChange}></TextField>
            <Container style={{display:"flex", justifyContent:"center", marginTop:"15px"}}>
                <Button color="warning" variant="contained" style={{width:"100%"}} onClick={onBtnSendFormHandler}>GỬI ĐƠN HÀNG</Button>
                {status ===true?<AlertDialog/>:null}
            </Container>
        </Container>
    )
}

export default OrderFormComponent;