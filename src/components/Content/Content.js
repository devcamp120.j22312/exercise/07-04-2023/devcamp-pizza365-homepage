import ComboComponent from "./combo.components";
import DrinkComponent from "./drink.components";
import OrderFormComponent from "./orderform.components";
import PizzaTypeComponent from "./pizzatype.components";
import SlideComponent from "./slide.components";
import Why365Component from "./why365.components";
import { useSelector } from "react-redux";

const Content = () => {
    const {orderId} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    return (
        <>
        <SlideComponent />
        <Why365Component />
        <ComboComponent />
        <PizzaTypeComponent />
        <DrinkComponent />
        <OrderFormComponent />
        </>
    )
}

export default Content;