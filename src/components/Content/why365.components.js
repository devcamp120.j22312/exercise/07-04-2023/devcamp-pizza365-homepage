import { Container, Grid, Typography } from "@mui/material";

const Why365Component = () => {
    return (
        <Container style={{ marginTop: "30px" }}>
            <Typography style={{ fontWeight: "700", textAlign: "center", color: "orange", fontSize: "28px" }}>
                TẠI SAO LẠI LÀ PIZZA 365
            </Typography>
            <hr style={{ color: "orange", width: "25%" }} />
            <Grid container style={{ marginTop: "20px", display: "flex", justifyContent: "space-around" }}>
                <Grid style={{ backgroundColor: "#FAFAD2", border: "1px solid #DFDFDF", margin: "10px", height: "195px", paddingLeft: "16px", width: "266px" }}>
                    <Typography style={{ width: "240px" }}>
                        <h2>Đa dạng</h2>
                        <p>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot hiện nay.</p>
                    </Typography>
                </Grid>
                <Grid item style={{ backgroundColor: "#FFFF00", border: "1px solid #DFDFDF", margin: "10px", height: "195px", paddingLeft: "16px", width: "266px" }}>
                    <Typography style={{ width: "240px" }}>
                        <h2>Chất lượng</h2>
                        <p>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                    </Typography>
                </Grid>
                <Grid item style={{ backgroundColor: "#FFA07A", border: "1px solid #DFDFDF", margin: "10px", height: "195px", paddingLeft: "16px", width: "266px" }}>
                    <Typography style={{ width: "240px" }}>
                        <h2>Hương vị</h2>
                        <p>Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</p>
                    </Typography>
                </Grid>
                <Grid item style={{ backgroundColor: "#FFA500", border: "1px solid #DFDFDF", margin: "10px", height: "195px", paddingLeft: "16px", width: "266px" }}>
                    <Typography style={{ width: "240px" }}>
                        <h2>Dịch vụ</h2>
                        <p>Nhân viên thân thiện, nhà hàng hiện đại, Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Why365Component;