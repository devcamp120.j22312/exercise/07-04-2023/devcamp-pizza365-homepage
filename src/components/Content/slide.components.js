import { Container, Paper } from "@mui/material";
import Carousel from "react-material-ui-carousel";

import pizza1 from '../../assets/images/1.jpg'
import pizza2 from '../../assets/images/2.jpg'
import pizza3 from '../../assets/images/3.jpg'
import pizza4 from '../../assets/images/4.jpg'

const SlideComponent = () => {
    var images = [pizza1, pizza2, pizza3, pizza4];

    const Item = (props) => {
        return (
            <Paper style={{boxShadow: "none"}}>
                <img src={props.item} alt="" style={{width: "100%"}}/>
            </Paper>
        )
    }

    return (
        <Container>
            <Carousel>
                {
                    images.map((item, index) => <Item key={index} item={item} />)
                }
            </Carousel>
        </Container>
    )
}

export default SlideComponent;