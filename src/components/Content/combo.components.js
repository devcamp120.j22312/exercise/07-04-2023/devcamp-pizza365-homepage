
import { Button, Container, Grid, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { PizzaSizeHandler } from "../../actions/task.actions";

const ComboComponent = () => {
    const dispatch = useDispatch();
    // B1: Nhận giá trị khởi tạo của state trong giai đọan đầu
    const {pizzaSize} = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    const onButtonPizzaSize = (id) => {
        dispatch(PizzaSizeHandler(id))
    }

    return (
        <Container style={{ marginTop: "30px" }}>
            <Typography style={{ fontWeight: "700", textAlign: "center", color: "orange", fontSize: "28px" }}>
                CHỌN SIZE PIZZA
            </Typography>
            <hr style={{width:"20%"}}/>
            <p style={{ fontWeight: "600", textAlign: "center", color: "#FFA500", fontSize: "18px", marginTop: "5px" }}>Chọn combo pizza phù hợp với nhu cầu của bạn</p>
            <Grid container style={{ display: "flex", justifyContent: "space-around" }}>
                {
                    pizzaSize.map((element, index) =>
                        <Grid item style={{ height: "auto", border: '1px solid #DFDFDF', textAlign: 'center', width: "335px", margin: "20px" }}>
                            <Typography style={{ height: "60px", backgroundColor: "orange", paddingTop: "20px", borderBottom: '1px solid #DFDFDF' }}>
                                <h2 style={{ margin: "auto" }}>{element.size} ({element.description})</h2>
                            </Typography>
                            <Typography style={{ height: "40px", borderBottom: '1px solid #DFDFDF' }}>
                                <p style={{fontSize: "18px"}}>Đường kính: <span style={{ fontWeight: "600", }}>{element.duongKinh}</span></p>
                            </Typography>
                            <Typography style={{ height: "40px", borderBottom: '1px solid #DFDFDF' }}>
                                <p style={{fontSize: "18px"}}>Sườn nướng: <span style={{ fontWeight: "600" }}>{element.suonNuong}</span></p>
                            </Typography>
                            <Typography style={{ height: "40px", borderBottom: '1px solid #DFDFDF' }}>
                                <p style={{fontSize: "18px"}}>Salad: <span style={{ fontWeight: "600" }}>{element.salad}</span></p>
                            </Typography>
                            <Typography style={{ height: "40px", borderBottom: '1px solid #DFDFDF' }}>
                                <p style={{fontSize: "18px"}}>Nước ngọt: <span style={{ fontWeight: "600" }}>{element.nuocNgot}</span></p>
                            </Typography>
                            <Typography style={{ height: "60px", borderBottom: '1px solid #DFDFDF' }}>
                                <p style={{fontSize: "32px", fontWeight:"600", marginTop:"10px"}}><span>{element.price} VNĐ</span></p>
                            </Typography>
                            <Grid style={{height:"50px"}}>
                                <Button onClick={() => onButtonPizzaSize(index)} style={{ height: "40px", width: "75%", marginTop:"5px", boxShadow:"none"}} variant="contained" color="warning">Chọn</Button>
                            </Grid>
                        </Grid>
                    )
                }
            </Grid>
        </Container>
    )
}

export default ComboComponent;