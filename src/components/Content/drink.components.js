import { Container, FormControl, InputLabel, MenuItem, Select, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { DrinkHandler } from "../../actions/task.actions";

const DrinkComponent = () => {
    const dispatch = useDispatch();

    const [drink, setDrink] = useState("");

    const onChangeDrink = (event) => {
        dispatch(DrinkHandler(event.target.value));
        setDrink(event.target.value);
    }

    return (
        <Container style={{ marginTop: "30px" }}>
            <Typography style={{ fontWeight: "700", textAlign: "center", color: "orange", fontSize: "28px" }}>
                CHỌN ĐỒ UỐNG
            </Typography>
            <hr style={{ width: "20%" }} />
            <p style={{ fontWeight: "600", textAlign: "center", color: "#FFA500", fontSize: "18px", marginTop: "5px" }}>Chọn thức uống phù hợp với nhu cầu của bạn</p>
            <FormControl fullWidth style={{marginTop:"20px"}}>
                <InputLabel>Chọn đồ uống</InputLabel>
                <Select onChange={onChangeDrink} value={drink}>
                    <MenuItem value={"Trà tắc"}>Trà Tắc</MenuItem>
                    <MenuItem value={"Coca"}>Coca cola</MenuItem>
                    <MenuItem value={"Pepsi"}>Pepsi</MenuItem>
                    <MenuItem value={"Lavie"}>Lavie</MenuItem>
                    <MenuItem value={"Fanta"}>Fanta</MenuItem>
                </Select>
            </FormControl>
        </Container>
    )
}

export default DrinkComponent;