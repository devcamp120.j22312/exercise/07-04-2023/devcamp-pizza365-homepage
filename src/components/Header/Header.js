import { Container, Grid } from "@mui/material";

const Header = () => {
    return (
        <Container maxWidth style={{padding:"0"}}>
            <Grid container style={{height: "50px", backgroundColor: "orange", padding: "15px 30px"}}>
                <Grid item xs={3} style={{fontWeight: "600", textAlign:"center"}}>
                    Trang chủ
                </Grid>
                <Grid item xs={3} style={{fontWeight: "600", textAlign:"center"}}>
                    Combo
                </Grid>
                <Grid item xs={3} style={{fontWeight: "600", textAlign:"center"}}>
                    Loại pizza
                </Grid>
                <Grid item xs={3} style={{fontWeight: "600", textAlign:"center"}}>
                    Gửi đơn hàng
                </Grid>
            </Grid>
            <Container>
            <h1>PIZZA 365</h1>
            <h2 style={{fontWeight:'500'}}>Truly Italian</h2>
            </Container>
        </Container>
    )
}

export default Header;