import { TASK_DRINK_HANDLER, TASK_PIZZA_SIZE_HANDLER, TASK_PIZZA_TYPE_HANDLER, TASK_SUBMIT_HANDLER, TASK_SEND_ORDER, TASK_FORM_HANDLER } from "../constants/task.constants"

export const PizzaSizeHandler = (id) => {
    return {
        type: TASK_PIZZA_SIZE_HANDLER,
        payload: id
    }
}

export const PizzaTypeHandler = (id) => {
    return {
        type: TASK_PIZZA_TYPE_HANDLER,
        payload: id
    }
}

export const DrinkHandler = (id) => {
    return {
        type: TASK_DRINK_HANDLER,
        payload: id
    }
}

export const FormHandler = (value) =>{
    return{
        type: TASK_FORM_HANDLER,
        payload: value
    }
}

export const SubmitFormHandler = () => {
    return {
        type: TASK_SUBMIT_HANDLER
    }
}

export const SendOrderHandler = (value) =>{
    return {
        type: TASK_SEND_ORDER,
        payload: value
    }
}
